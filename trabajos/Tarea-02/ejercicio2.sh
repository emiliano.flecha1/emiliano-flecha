#!/bin/bash
####################################################
# EJERCICIO 2
##Este script captura el login de los usuarios y los guarda en un fichero 
###################################################

#Declaro ubicacion de los archivos
LOGS2=/tmp/login.log
Log_unico=/home/$USER/loginlocal.log 
echo " $LOGS2"

#Guardo los logs generales de todos los inicios de usuarios en ubicacion general.
timestamp=$(lastlog >> $LOGS2)
echo $timestamp
#Guardo los logs del usuario en el registro del mismo usuario
lastlog -u $USER >> $Log_unico

