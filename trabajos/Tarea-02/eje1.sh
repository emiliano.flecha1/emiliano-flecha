#!/bin/bash
####################################################
# EJERCICIO 1
##Este script crea backups del directorio de usuario. 
###################################################

#Creo variable de alojamiento del log. 
LOGS=/tmp/informe.log


Dest_log="/tmp"

#Defino variable de backup

BackUpUser="/home/$USER/"

# Tomo date de Inicio de backuop y la escribo en un archivo

echo "Hora de inicio del backup $(date +%T-%F)" | tee -a ficheroej1.txt

#Creo nombre del archivo
file=$(date +%T-%F)


#Realizo backup
tar -czf $Dest_log/$file.tar $BackUpUser 2> $LOGS


# Tomo date de Finalizacion de backuop y la agrego
#como linea al archivo anterior.
echo "Hora de finalizacion del backup $(date +%T-%F)" | tee -a ficheroej1.txt






