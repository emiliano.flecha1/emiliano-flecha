#######################
#Para el funcionamiento de este script debe tener
#permisos de root su -
# Consiste en 6 puntos de menu>
#Permisos Cambiar_owner Crear_grupos Listar_grupos Busqueda salir;
#Los primeros 3 precisan dos argumentos que se detallan arriba de cada funcion.
#El 4to no precisa argumentos
#El 5to precisa 1 argumento tambien indicado encima de la funcion.
#El ultimo es la salida.
############################
#!/bin/bash
if [ $UID -ne 0 ]; then
	echo "Ejecute este script como root para continuar"
	exit
fi


#Esta funcion se ejecuta con dos argumentos dados por el user
#Objetivo> Cambiar permisos de un directorio o archivo
#primer argumento archivo, segundo argumento mascara del archivo o directorio
permiso_fd () {
	echo "Carpeta a  modificar permisos $1"
	echo "Mascara a colocar rwx $2"
	if [ $# -eq 2 ]; then
		#cambio de permiso
		chmod $2 $1
		ls -l
	else
		#Instructivo
		printf $'El argumento de este script debe enviar dos argumentos\nEl primero debe ser el directorio o archivo que ser debe modificar permisos \nEl segundo la mascara de permisos a colocar\n'
	fi
	exit
}

#Esta funcion se ejecuta con dos argumentos dados por el user
#Objetivo> Cambiar el propietario de un directorio o archivo
#primer argumento archivo, segundo argumento nombre del nuevo propietario
owner_change () {
	if [ $# -eq 2 ]; then
		#cambio de permiso
		chown $2 -R $1
		ls -l
	else
		#Instructivo
		printf $'Para el funcionamiento de este script debe enviar dos argumentos\nEl primero debe ser la carpeta a modificar el owner\nEl segundo el nombre de usuario del nuevo owner\n'
	fi
	exit
}


create_group(){

	test $1 || echo "Debe ingresar de argumento el nombre del grupo a crear o eliminar"
	test $1 || exit

	read -p "Si desea crear un grupo marque 0, si desea eliminarlo 1: " n1
	if [ $n1 -eq 0 ]; then
		su - -c "groupadd $1"
		exit

	fi

	if [ $n1 -eq 1 ]; then
		su - -c "groupdel $1"
		exit

	else
		exit
	fi


}


#Esta funcion tiene solo el objetivo de listar los grupos
group_list(){
	getent group
	exit
}

#esta funcion busca el nombre de un archivo en todo el sistema.
search_file(){
	test $1 || echo "Debe ingresar el nombre del archivo a buscar"
	test $1 || exit
	find / -name $1
	exit 0
}

#Menu con sus opciones y respectivas llamadas
PS3="Selecciona una operación: "
select opt in Permisos Cambiar_owner Crear_grupos Listar_grupos Busqueda salir;
do
	case $opt in
		Permisos) permiso_fd $1 $2;;
		Cambiar_owner) owner_change $1 $2;;
		Crear_grupos) create_group $1;;
		Listar_grupos) group_list "";;
		Busqueda) search_file $1;;
		salir) break;;
		*) echo "$REPLY es una opción inválida";;
	esac
done

