#!/bin/bash
  ###############Obteniendo politica############
  echo "Bienvenido al proceso de alta de UFW\n
  Siga las instrucciones\n
  "
  echo -e "Que Politica desea utilizar\n
  1. ALLOW
  2. DENY
  3. REJECT"
  read opt_ch
  case $opt_ch in
	  1) polit="allow" ;;
	  2) polit="deny" ;;
	  3) polit="reject" ;;
	  *) echo -e "Seleccion incorrecta!!!"
  esac
   
    ###############Origen############
    echo -e "
    1. Firewall IP ORIGEN\n
    2. Firewall subnet de origen\n
    3. Firewall usar TODOS de origen\n
    "
    fe_origen=0
    read opt_ip_or
    case $opt_ip_or in
	    1) echo -e "\nIngrese IP "
		    read ip_source ;;
	    2) echo -e "\nPor favor ingrese Subnet (e.g 192.168.10.0/24)"
		    read ip_source ;;
	    3) ip_source="any"
		    fe_origen=1 ;;
	    *) echo -e "Seleccion incorrecta!!!"
    esac 

      #########Getting Destination IP Address##########
      echo -e "
      1. Firewall IP DESTINO\n
      2. Firewall subnet de DESTINO\n
      3. Firewall usar TODOS de DESTINO\n
      "
      fe_destino=0
      read opt_ip_des
      case $opt_ip_des in
	      1) echo -e "\nIngrese IP (192.168.2.2)"
		      read ip_dest ;;
	      2) echo -e "\nPor favor ingrese subnet de destino (e.g 192.168.10.0/24)"
		      read ip_dest ;;
	      3) ip_dest="any"
		      fe_destino=1 ;;
	      #4) ip_dest = "NULL" ;;
	      *) echo -e "Seleccion incorrecta"
      esac
        ###############Getting the Protocol#############
	echo -e "
	1. Sobre puerto especifico 
	2. Utilizar nombre de servicio sin origen y destino
	"
	read proto_ch
	case $proto_ch in
		1) echo -e "Ingrese el numero de puerto: "
			serv=0
			read proto ;;
		2) echo -e "Ingrese el nombre de servicio:"
			serv=1
			fe_destino=0
			fe_origen=0
			read proto ;;
		*) echo -e "Wrong option Selected!!!"
	esac



	  #############Que hace la Regla?############# 
	  read -p "Elija cualquier tecla para crear regla: " tecla
	  echo -e "La regla a crear es: \n"
	  if [ $serv == 1 ]; then
		  #ufw deny from 11.12.13.0/24 to any port 80
		  echo -e "\nufw $polit $proto\n"
		  gen=1
		  echo -e "\n\tQuieres CREAR esta regla , Si=1 , No 2"
		  read sino

		  if [ $sino == 1 ]; then
			  ufw $polit $proto
			  exit
		  else if [ $sino == 2 ]; then
			  clear
			  echo "Go to main"
			  exit
			  fi
			  fi



		  else if [ $serv == 0 ]; then
			  echo -e "La regla a crear es\n 
			  "
			  echo -e "\nufw $polit from $ip_source to $ip_dest port $proto\n 
			  "

			  echo -e "\n\tQuieres CREAR esta regla , Si=1 , No 2"
			  read sino

			  if [ $sino == 1 ]; then
				  ufw $polit from $ip_source to $ip_dest port $proto
				  exit

			  else if [ $sino == 2 ]; then
				  clear
				  echo "Go to main"
				  exit
				  fi
				  fi

				  fi
				  fi
