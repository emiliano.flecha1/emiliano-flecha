#!/bin/bash
##############UFW FIREWALL SCRITP ########
echo -e "****************Welcome*************"

#CAMBIAR POLITICAS POR DEFECTO.
#ELEGIR CADENA A CAMBIAR
#ELEGIR POLITICA




defaultpolicies () {
	echo -e "
	###########Ejemplo de ingreso de informacion#######\n
	NOMENCLATURA>  ufw default <policy> <chain> command\n
	Ejemplo>       ufw default allow outgoing\n
	"

	echo "DATOS VALIDOS COMO POLITICA: ALLOW DENY REJECT\n
	DATOS VALIDOS COMO CADENA : OUTGOING, INCOMING, ROUTED\n
	"


	read -p "Escriba el dato de POLITICA: " n1
	read -p "Escriba INCOMING, OUTGOING O ROUTED: " n2

	echo "Si algun dato se encuentra vacio se saldra de la funcion\n"

	test $n1 || exit
	test $n2 || exit

	echo -e "ufw default $n1 $n2"
	#echo -e "\niptables -A $chain -s $ip_source -d $ip_dest -p $proto -j $rule\n"
	echo -e "\n\tQuieres ejecutar la regla indicada arriba en el UFW? Si=1 , No=2"
	read sino

	if [ $sino == 1 ]; then
		ufw default $n1 $n2

	else if [ $sino == 2 ]; then
		clear
		echo "Go to main"
		exit
	fi
	fi
}


abm () {


	#ufw delete 7

	#!/bin/bash
	###############OPCIONES DE ABM ############
	  
	opt_abm=1
	while [ $opt_abm != 3 ]
	do
		echo -e "Que desea realizar?\n
		1. ALTA
		2. BAJA
		3. EXIT"
		read opt_abm
		case $opt_abm in
			1) abm="ALTA" ;;
			2) abm="BAJA" ;;
			3) abm="EXIT" ;;
			*) echo -e "Seleccion incorrecta!!!"
		esac
		 
		if [ $abm == ALTA ]; then
			echo "entramos en alta\n
			"
			bash alta.sh 
			echo "\nLas reglas quedaron de la siguiente forma:\n
			"
			ufw status numbered

		else if [ $abm == BAJA ]; then
			echo "Salimos de baja\n
			"
			bash baja.sh 
			echo "\nLas reglas quedaron de la siguiente forma:\n
			"
			ufw status numbered
		else if [ $abm == EXIT ]; then
			echo "Salimos de EXIT\n
			"
			opt_abm=3
			fi
			fi
			fi
		done
		exit 0
	}




showrules () {
	#########Mostrar reglas#########
	#iptables -L --line-numbers
	ufw status numbered


} 

backuprules () {
	#########backup de reglas#########
	date| tee -a /tmp/informe.log
	ufw status numbered| tee -a /tmp/informe.log

	echo "el backup se ubica en el siguiente directorio /tmp/informe.log "


	 #Mover de posicion reglas
	   #  - Mostrar reglas
	      # - Crear un backup de reglas
	          #- Permitir/denegar que el firewall se cargue con el sistema

	  } 

  main(){

	  ROOT_UID=0
	  if [ $UID == $ROOT_UID ];
	  then
		  clear

		  opt_main=1
		  while [ $opt_main != 6 ]
		  do
			  echo -e "/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\n" 
			  #############UFW  MAIN MENU ############ 
			  echo -e "\t*****Main Menu*****\n
			  1. Cambiar politicas por defecto
			  2. ABM de reglas
			  3. Mover reglas
			  4. Mostrar Reglas
			  5. backup de reglas
			  6. Permitir/denegar que el firewall se cargue con el sistema
			  7. Salir"
			  read opt_main
			  case $opt_main in
				  1) defaultpolicies ;;
				  2) abm ;;
				  3) moverules ;;
				  4) showrules ;;
				  5) backuprules ;;
				  6) permit_deny ;;
				  7) exit 0 ;;

				  *) echo -e "Wrong option Selected!!!"
			  esac
		  done


	  else
		  echo -e "OPS! Tienes que ser root para continuar"
		  fi
	  }

  main
  exit 0
