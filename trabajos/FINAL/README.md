#!/bin/bash
#
# Final programming exam 
# 
#                                   PROGRAM  :STATUS SO, CONFIGURATION AND REPORTING
#                                                       DEBIAN
#
# AUTHOR: Emiliano Flecha
#
# To use this script you can do it without the need for arguments directly from the menu.

---------

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

The Administration Menu module displays the entire administrative menu tree
(and most local tasks) in a drop-down menu, providing administrators one- or
two-click access to most pages.  Other modules may also add menu links to the
menu using hook_admin_menu_output_alter().

 * For a full ACCESS, visit the project page:
   https://gitlab.com/emiliano.flecha1/emiliano-flecha



REQUIREMENTS
------------

-This module requires no modules outside 


INSTALLATION
------------
-The script will take care of the installation of all the programs necessary for its operation.

-No user action required

CONFIGURATION
-------------
 
-Read carefully the instructions within each function.

-All administration categories require root permission to be accessible.
-The administration menu will be empty unless you have this permission

-Users with this permission will see the administration menu at the top of each function


TROUBLESHOOTING
---------------

 * If the menu does not appear, check the following:

- Use ROOT permissions for its execution.
- Use the "Access Menu" without sending arguments. It is an interactive menu with the user.

FAQ
---

Q: The reports are generated in isolation and in the folder where I am executing the script, 
   is this normal?

A:Yes, this is the intended behavior. The program takes care of its categorization 
  in different folders to facilitate its download.


MAINTAINERS
-----------

Current maintainers:
 * Emiliano Flecha (analyst) - https://gitlab.com/emiliano.flecha1/emiliano-flecha


This project has been carried out under the instructions of the teacher: Sergio Pernas
Professor at ISTEA (SUPERIOR TECHNOLOGICAL INSTITUTE ARGENTINE BUSINESS)
