#!/bin/bash
############## CONFIGURACION Y REPORTERIA DE VIRTUALES DEBIAN ########
#
#ESTE SCRIPT TIENE COMO OBJETIVO OBTENER INFORMACION DE SO Y SU PREPARACION
echo "///////////////////////////////////////////////////////////////////"
echo "| ----------------------------------------------------------------|"
echo "|               *STATUS SO, CONFIGURATION AND REPORTING*          |"
echo "|               (Solo el usuario root, tiene permisos)            |"
echo "|---------------------------------------------------------------- |"

ROJO=$(tput setaf 1)
VERDE=$(tput setaf 2)
AMAR=$(tput setaf 3)
AZUL=$(tput setaf 4)
VIOLET=$(tput setaf 5)
CYAN=$(tput setaf 6)
BLANCO=$(tput setaf 7)
VOLVER=$(tput sgr0)



Add_host () {

	echo -e "${CYAN}CAMBIO DE HOSTNAME${VOLVER}"
	echo ""
	echo -e "ingrese nuevo nombre del host\n"
	read -p "Nuevo nombre de host: " nombrehost

	echo -e "El nombre de host ingresado es $nombrehost"

	echo -e "Desea configurar ese hostname\n 
	SI =1 NO= 2\n"

	read -p "ingrese opcion:" sino

	if [ $sino == 1 ]; then

		hostnamectl set-hostname $nombrehost

		echo -e "Nuevo hostname $nombrehost"

		echo -e "Recuerde que para que el este hostname quede aplicado se debe reiniciar el sistema"


	else if [ $sino == 2 ]; then
		clear
		echo "Go to main"

	fi
	fi

	exit 0

}


Red () {
	#USO DE VARIABLES ESPECIALES
	#INSTALACION DE PAQUETES NECESARIOS
	echo -e "${CYAN}Preparando informe de Red del SO${VOLVER}"
	echo ""
	sleep 1
	echo "Antes de comenzar precisamos instalar unas herramientas......"
	sleep 1
	dpkg -l | grep -v "^rc" | grep net-tools &> /dev/null
	if [ $? -ne 0 ]; then
		read -p " Usted no tiene netstat en su sistema el cual es necesario. Se instalara, presione enter para continuar..."
		apt-get update
		apt install net-tools

	else
			echo ""
			        echo "Ya tiene instalado net-tools"

	fi

	#awk '/enp0s8/{ print NR; exit }' /etc/network/interfaces

	#echo "Hostname : $(hostname -s)"
	#echo "Dominio DNS : $(cat /etc/resolv.conf | grep 'nameserver')"
	FECHA=$(date +"%d-%m-%Y_%H:%M:%S")
	test -d Reportes_red || mkdir Reportes_red
	REP_RED=Reportes_red/Reporte_$USER-$FECHA.txt
	touch $REP_RED



	echo "GENERANDO REPORTE..."

	echo -ne "${VERDE} *****                     (33%)\r${VOLVER}"
	sleep 1
	echo -ne "${VERDE}*************             (66%)\r${VOLVER}"
	sleep 1
	echo -ne "${VERDE}***********************   (100%)\r${VOLVER}"
	echo -ne '\n'

	echo "###########################################################################" >> $REP_RED
	echo "" >> $REP_RED
	echo "##################### REPORTE DE RED ######################################" >> $REP_RED
	echo "" >> $REP_RED
	echo "***********************" >> $REP_RED
	echo "******* HOSTNAME ******" >> $REP_RED
	echo "***********************" >> $REP_RED
	echo "Hostname : $(hostname -s)" >> $REP_RED
	echo "" >> $REP_RED
	echo "***********************" >> $REP_RED
	echo "******* Dominio *******" >> $REP_RED
	echo "***********************" >> $REP_RED
	echo "Dominio DNS : $(cat /etc/resolv.conf | grep 'nameserver')" >> $REP_RED
	echo "" >> $REP_RED
	echo "***********************" >> $REP_RED
	echo "***Enrutamiento  Red***" >> $REP_RED
	echo "***********************" >> $REP_RED
	netstat -nr >> $REP_RED
	echo "" >> $REP_RED
	echo "**************************************" >> $REP_RED 
	echo "**Informacion Trafico de la Interfaz**" >> $REP_RED
	echo "**************************************" >> $REP_RED
	netstat -i >> $REP_RED
	echo "" >> $REP_RED
	echo "**************************************" >> $REP_RED 
	echo "**Conexiones establecidas**" >> $REP_RED
	echo "**************************************" >> $REP_RED
	netstat -natu | grep 'ESTABLISHED' >> $REP_RED
	echo "" >> $REP_RED
	echo "**************************************" >> $REP_RED 
	echo "**Listening connections**" >> $REP_RED
	echo "**************************************" >> $REP_RED
	netstat -an | grep 'LISTEN' >> $REP_RED
	echo "###########################################################################" >> $REP_RED
	sleep 2
	echo "El reporte ha sido generado en la carpeta\n
donde se ejecuto este script"
sleep 3
}


USR_ADD () {

	echo -e "${CYAN}AGREGAR USUARIOS${VOLVER}"
	echo ""
	#USO DE ARRAY
	array_user=()
	opt_usr=1
	while [ $opt_usr != 2 ]
	do
		read -p "ingrese nombre de usuario  " usr
		sleep 1
		echo "Agregando usuario"
		useradd -s /bin/bash -d /home/$usr -m $usr                   
		echo " Generado Usuario: $usr"                              
		echo "Ingrese Contraseña o cambie contraseña de este usuario"   
		echo ""
		passwd $usr
		echo ""
		echo "Usuario agregado $usr"
		echo ""
		array_usr+=($usr)
		echo "USUARIOS GENERADOS: ${array_usr[@]}"
		echo ${array_usr[1]}
		echo ${array_usr[0]}
		echo ""
		echo ""
		echo -e "Desea agregar otro usuario\n
		SI= 1
		NO= 2"
		echo ""
		read -p "ingrese el numero que corresponda: " sino
		if [ $sino == 1 ]; then
			opt_usr=1

		else if [ $sino == 2 ]; then
			opt_usr=2

		else
			exit
		fi
		fi
	done

	#NOTAS DE APRENDIZAJE
	#useradd -s /bin/bash -d /home/$USRNAME -m $USRNAME
	#echo $contrasena | passwd --stdin $NEWUSER >& /dev/null
	#useradd -s /bin/bash -d /home/$NEWUSER -m $NEWUSER >& /dev/null ;  echo -e "$PASSWORD\n$PASSWORD" >& /dev/null |  passwd $NEWUSER >& /dev/null
	#useradd -s /bin/bash -d /home/fede -m fede >& /dev/null 
	#echo "password" | passwd --stdin "fede" >& /dev/null
	#useradd -s /bin/bash -d /home/vale -m vale >& /dev/null ;  echo -e "pepe\npepe" >& /dev/null |  passwd vale >& /dev/null
	#TIP para leer status contrasenas
	#passwd -S javier
	#javier P 08/16/2012 0 30 5 -1
	#Las opciones que tienen las contraseñas son:
	#L  -> Contraseña bloqueada
	#NP -> Sin contraseña
	#P  -> Contraseña válida
}


USR_DEL () {
	echo -e "${CYAN}ELIMINACION DE USUARIOS${VOLVER}"
	echo ""
	array_user=()
	opt_usr=1
	while [ $opt_usr != 2 ]
	do	
		echo ""
		read -p "Que usuario va a eliminar? " usrdel
		echo ""
		echo "Eliminando usuario..."
		sleep 1
		userdel $usrdel
		if [ $? -ne 0 ]; then
			echo ""
			echo "${ROJO}El usuario indicado no existe${VOLVER}"
			echo ""
		else
			echo -e "\n
			Usuario eliminado con exito"	
			array_usr+=($usrdel)
		fi
		echo -e "\n
		Desea ELIMINAR otro usuario\n
		SI= 1
		NO= 2"
		read -p "ingrese si o no segun corresponda " sino
		if [ $sino == 1 ]; then
			opt_usr=1

		else if [ $sino == 2 ]; then
			opt_usr=2
			echo -e "Total de usuarios eliminados> ${array_usr[@]} "

		else
			exit
		fi
		fi
	done
}


REP_USR() {
	echo -e "${CYAN}REPORTE DE USUARIOS${VOLVER}"
	echo ""
	HOST=$(hostname)
	FECHA=$(date +"%d-%m-%Y_%H:%M:%S")
	test -d Reportes_user || mkdir Reportes_user
	REP_USERS=Reportes_user/Reporte_$HOST-$FECHA.txt
	touch $REP_USERS



	echo "${CYAN}COMENZANDO REPORTE...${VOLVER}"

	echo -ne "${VERDE}*****                     (33%)\r${VOLVER}"
	sleep 1
	echo -ne "${VERDE}*************             (66%)\r${VOLVER}"
	sleep 1
	echo -ne "${VERDE}***********************   (100%)\r${VOLVER}"
	echo -ne '\n'

	echo "################################################################################" >> $REP_USERS
	echo "" >> $REP_USERS
	echo "##################### REPORTE DE USUARIOS ######################################" >> $REP_USERS
	echo "" >> $REP_USERS
	echo "***********************" >> $REP_USERS
	echo "******* HOSTNAME ******" >> $REP_USERS
	echo "***********************" >> $REP_USERS
	echo "Hostname : $(hostname -s)" >> $REP_USERS
	echo "" >> $REP_USERS
	echo "**********************************" >> $REP_USERS
	echo "******* Cantidad de usuarios *****" >> $REP_USERS
	echo "**********************************" >> $REP_USERS
	echo "" >> $REP_USERS
	cut -d: -f1 /etc/passwd | wc -l >> $REP_USERS
	#fila 1 del /etc/passwd, y contamos
	echo "" >> $REP_USERS
	echo "****************************************" >> $REP_USERS
	echo "********** Listado de usuarios *********" >> $REP_USERS
	echo "********* con acceso /bin/bash *********" >> $REP_USERS
	echo "****************************************" >> $REP_USERS
	echo "" >> $REP_USERS
	#lISTADO DE USUARIOS
	cat /etc/passwd | grep '/bin/bash' | cut -d":" -f1 >> $REP_USERS
	echo "" >> $REP_USERS
	echo "**********************************" >> $REP_USERS
	echo "******* Listado de usuarios ******" >> $REP_USERS
	echo "********* Sin contraseña *********" >> $REP_USERS
	echo "**********************************" >> $REP_USERS
	echo "" >> $REP_USERS
	getent shadow | grep -Po '^[^:]*(?=:.?:)' >> $REP_USERS
	echo "" >> $REP_USERS
	echo "********************************************" >> $REP_USERS
	echo "********* Ultimas conexion de cada **********" >> $REP_USERS
	echo "********* usuario en el ultimo mes *********" >> $REP_USERS
	echo "********************************************" >> $REP_USERS
	echo "" >> $REP_USERS
	last -s '-1 month'  | sort -u -k1,1 >> $REP_USERS
	echo "" >> $REP_USERS


	echo "${BLANCO}Obteniendo informacion de sistema${VOLVER}"
	sleep 1 
	echo -ne "${VERDE}*************             (66%)\r${VOLVER}"
	sleep 1
	echo -ne "${VERDE}***********************   (99%)\r${VOLVER}"
	sleep 1
	echo -ne "${VERDE}***********************   (100%)\r${VOLVER}"
	echo -ne '\n'
	echo "${VERDE}REPORTE FINALIZADO${VOLVER}"
	echo -e "
	Puede visualizar el mismo
	en la carpeta Reportes_user"
	sleep 1

}


Access_center () {

	echo "${CYAN}################################################################${VOLVER}" 
	echo "${CYAN}################### Portal de accesos ##########################${VOLVER}"
	echo "${CYAN}################################################################${VOLVER}" 
	echo ""

	opt_portal=1
	while [ $opt_portal != 4 ]
	do
		echo -e "/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\n" 
		############# Portal MENU ############ 
		echo -e "\t*****PORTAL USER & GROUP MENU*****\n
		1. Creacion de usuarios
		2. Eliminacion de usuarios
		3. Reporte de usuarios
		4. Salir"
		read opt_portal
		case $opt_portal in
			1) USR_ADD ;;
			2) USR_DEL ;;
			3) REP_USR ;;
			4) exit 0 ;;

			*) echo -e "Wrong option Selected!!!"
		esac
	done

}


PER_ADD () {
		
	sino=1
	while [ $sino != 2 ]
	do
		echo "${CYAN}Funcion de agregacion de permisos${VOLVER}"
		echo ""
		echo -e "
		Sobre que archivo o directorio desea agregar permisos?

		Favor detallar ruta completa

		Ej /home/$USER/trabajos/archivo.txt"
		echo ""
		read -p "Archivo a modificar permisos " file
		echo ""
		echo "Procesando...."
		sleep 1
		echo ""
		clear
		echo -e "
		${BLANCO}Ejemplo de asignacion de permisos

		0 = --- = sin acceso
		1 = --x = ejecución
		2 = -w- = escritura
		3 = -wx = escritura y ejecución
		4 = r-- = lectura
		5 = r-x = lectura y ejecución
		6 = rw- = lectura y escritura
		7 = rwx = lectura, escritura y ejecución

		Permiso ejemplo> -rw-rw-r--  1 lorenzo lorenzo  11K dic  2 19:06 feed.log

		El primero grupo de izquierda a derecha pertenece al propietario
		El segundo grupo al Grupo Propietario
		El tercer grupo a los demas

		400 Lectura solo al propietario
		040 Lectura solo al grupo
		004 Lectura solo a los demas

		${VOLVER}"
		echo "Que permisos desea asignarle a $file ?"
		echo ""
		echo "Escribir solo los numeros de Mascara. Ej 666"
		echo ""
		read -p "" mask
		echo -e "

		"
		chmod $mask $file || echo "${ROJO}Fallo la ejecucion de Permisos${VOLVER}"
		echo ""
		echo -e "La configuracion realizada fue la siguiente\n

		chomod $mask $file 

		"
		echo "${VIOLET}Desea Cambiar otro permiso?${VOLVER}"
		echo -e "Seleccione
		SI= 1
		NO= 2
		"
		read -p "Ingrese su opcion " sino
		#sino=$valor
	done

}

PROP_ADD ()
{
	#ROJO=$(tput setaf 1)
	#VERDE=$(tput setaf 2)
	#AMAR=$(tput setaf 3)
	#AZUL=$(tput setaf 4)
	#VIOLET=$(tput setaf 5)
	#CYAN=$(tput setaf 6)
	#BLANCO=$(tput setaf 7)
	#VOLVER=$(tput sgr0)

	folder=()
	opt_prop=1
	while [ $opt_prop != 2 ]
	do
		echo "${CYAN}##############Funcion de definicion de PROPIETARIO ###########${VOLVER}"
		echo ""
		echo -e "
		Esta herramienta tiene como funcion gestionar el propietario 
		de un archivo o directorio"
		echo ""
		echo -e "
		Indique el nombre de usuario del nuevo propietario\n
		Ej $USER"
		echo ""
		read -p "${BLANCO}Nombre de usuario${VOLVER} " usuario 
		echo ""
		bucle=1
		while [ $bucle != 2 ]
		do
			echo -e "
			DETALLE EL ARCHIVO O DIRECTORIO QUE LE QUIERE ASIGNAR AL PROPIETARIO SELECCIONADO

			Favor detallar ruta completa en caso de estar en una carpeta diferente 
			a la que usted esta situado.

			Ej /home/$USER/trabajos/archivo.txt

			"	
			read -p "${BLANCO}Ruta o nombre${VOLVER}  " ruta
			echo ""
			folder+=($ruta)

			echo "Desea agregar otra ruta mas para este propietario? "
			echo ""
			echo -e "
			Seleccione:
			Si = 1       No =2"
			read -p "${BLANCO}Seleccion>  ${VOLVER}" bucle
		done
		echo ""
		echo "${VERDE}Procesando Solicitud${VOLVER}"
		sleep 1
		echo ""
		echo -e "
		EJECUTANDO  comando

		chown $usuario ${folder[@]} 

		"

		chown $usuario ${folder[@]} || echo "${ROJO}Fallo la ejecucion de PROPIETARIO\n Revise que los datos ingresados sean correctos\n segun los ejemplos dados ${VOLVER}"

		sleep 1 
		echo -ne "${VERDE}*****                     (25%)\r${VOLVER}"
		sleep 1
		echo -ne "${VERDE}*************             (60%)\r${VOLVER}"
		sleep 1
		echo -ne "${VERDE}***********************   (100%)\r${VOLVER}"
		echo -ne '\n'
		echo "${VERDE}CAMBIO DE PROPIETARIO FINALIZADO${VOLVER}"
		echo -e "

		"
		echo "Desea realizar otro cambio?"
		echo -e "
		Seleccione:
		Si = 1       No =2"
		read -p "${BLANCO}Seleccion>  ${VOLVER}" opt_prop
		folder=()
	done

}


Permits () {

	echo "${CYAN}Bienvenido a la asignacion de permisos${VOLVER}"
	echo ""
	echo "${CYAN}################################################################${VOLVER}" 
	echo "${CYAN}################### Portal de PERMISOS #########################${VOLVER}"
	echo "${CYAN}################################################################${VOLVER}" 
	echo ""

	opt_per=1
	while [ $opt_per != 3 ]
	do
		echo -e "/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\n" 
		############# PERMISOS MENU ############ 
		echo -e "\t*****PERMISOS Menu*****\n
		1. Permisos a directorios y archivos
		2. Propietarios
		3. Salir"
		read opt_per
		case $opt_per in
			1) PER_ADD ;;
			2) PROP_ADD ;;
			3) exit 0 ;;

			*) echo -e "Wrong option Selected!!!"
		esac
	done
	#chown user02 file1
	#chown :groupA file1
	#chown user02:groupA file2
}

FIREWALL_PREP () {
	echo "${CYAN} Preparacion de firewall ${VOLVER}"
	echo ""
	echo ""

	#Habilitacion de conexion
	iptables -P INPUT ACCEPT              
	iptables -P FORWARD ACCEPT
	iptables -P OUTPUT ACCEPT
	# Flush all current rules from iptables
	#
	iptables -F
	iptables -t nat -F
	clear
	sleep 1 
	echo "${BLANCO}Configurando puertos permitidos${VOLVER}"
	echo -ne "${VERDE}*****                     (33%)\r${VOLVER}"
	sleep 1
	echo -ne "${VERDE}*************             (66%)\r${VOLVER}"
	sleep 1
	echo -ne "${VERDE}***********************   (100%)\r${VOLVER}"
	echo -ne '\n'
	echo ""
	echo "${BLANCO}Configurando conexiones establecidas${VOLVER}"
	echo ""
	echo -ne "${VERDE}*****                     (33%)\r${VOLVER}"
	sleep 1
	echo -ne "${VERDE}*************             (66%)\r${VOLVER}"
	sleep 1
	echo -ne "${VERDE}***********************   (100%)\r${VOLVER}"
	echo -ne '\n'
	echo ""
	echo "${BLANCO}Configurando politicas por default${VOLVER}"
	echo ""
	echo ""
	echo -ne "${VERDE}*****                     (33%)\r${VOLVER}"
	sleep 1
	echo -ne "${VERDE}*************             (66%)\r${VOLVER}"
	sleep 1
	echo -ne "${VERDE}***********************   (100%)\r${VOLVER}"
	echo -ne '\n'
	echo ""
	echo "${VERDE}
	CONFIGURACION DE FIREWALL FINALIZADA
	${VOLVER}"
	sleep 3
	echo ""
	#PUERTOS PERMITIDOS 
	# ICMP (Ping)
	iptables -t filter -A INPUT -p icmp -j ACCEPT
	iptables -t filter -A OUTPUT -p icmp -j ACCEPT
	# SSH
	iptables -t filter -A INPUT -p tcp --dport 22 -j ACCEPT
	iptables -t filter -A OUTPUT -p tcp --dport 22 -j ACCEPT
	iptables -t filter -A FORWARD -p tcp --dport 22 -j ACCEPT
	# Git
	iptables -t filter -A OUTPUT -p tcp --dport 9418 -j ACCEPT
	iptables -t filter -A INPUT -p tcp --dport 9418 -j ACCEPT
	# DNS
	iptables -t filter -A OUTPUT -p tcp --dport 53 -j ACCEPT
	iptables -t filter -A OUTPUT -p udp --dport 53 -j ACCEPT
	iptables -t filter -A INPUT -p tcp --dport 53 -j ACCEPT
	iptables -t filter -A INPUT -p udp --dport 53 -j ACCEPT
	# PROTOCOLOS DE MAIL
	iptables -t filter -A INPUT -p tcp --dport 25 -j ACCEPT
	iptables -t filter -A OUTPUT -p tcp --dport 25 -j ACCEPT
	iptables -t filter -A INPUT -p tcp --dport 110 -j ACCEPT
	iptables -t filter -A OUTPUT -p tcp --dport 110 -j ACCEPT
	iptables -t filter -A INPUT -p tcp --dport 143 -j ACCEPT
	iptables -t filter -A OUTPUT -p tcp --dport 143 -j ACCEPT
	#CONFIGURACION DE HORARIO VIA SERVER NTP
	iptables -t filter -A OUTPUT -p udp --dport 123 -j ACCEPT


	# ACCEPT DE LOCALHOST 
	iptables -A INPUT -i lo -j ACCEPT



	# AUTORIZAMOS CONEXIONES ESTABLECIDAS 
	iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
	iptables -A FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT
	iptables -A OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
	iptables -t filter -A INPUT -i lo -j ACCEPT
	iptables -t filter -A OUTPUT -o lo -j ACCEPT

	# POLITICAS POR DEFECTO EN DROP
	#
	iptables -P INPUT DROP              
	iptables -P FORWARD DROP
	iptables -P OUTPUT DROP

	echo "Mostrando configuracion en pantalla :"
	echo ""
	# Comprobamos cómo quedan las reglas
	iptables -L -n

}



FIREWALL_CUSTOM () {

	echo "${CYAN} Bienvenido al creador de politicas CUSTOM${VOLVER}"
	echo ""
	echo " Desea modificar las politicas por defecto, especificar una politca personalizada, o eliminar? "
	echo ""
	echo -e "${BLANCO}
	Seleccione segun corresponda:
	Defecto = 1
	Especificar = 2 
	ELIMINAR = 3 ${VOLVER}"
	echo ""
	read -p "" opt_pol 

	if [ $opt_pol == 1 ]; then 

		echo ""
		echo "Creandor de reglas Default"
		echo ""
		echo -e "${BLANCO}Que cadena desea utilizar\n
		  1. INPUT
		    2. OUTPUT
		      3. Forward${VOLVER}"
		        read -p " " opt_ch
			  case $opt_ch in
				     1) chain="INPUT" ;;
				        2) chain="OUTPUT" ;;
					   3) chain="FORWARD" ;;
					      *) echo -e "Seleccion incorrecta!!!"
						        esac
							echo ""
							echo ""
							echo "A continuacion...."
							echo -e "${BLANCO}Que cadena desea utilizar\n
							1. ACCEPT
							2. DROP${VOLVER}"
							read -p " " opt_policy
							case $opt_policy in
								1) policy="ACCEPT" ;;
								2) policy="DROP" ;;
								*) echo -e "Seleccion incorrecta!!!"
							esac
							echo ""
							echo ""
							echo "${AMAR}La politica a crear es la siguiente:${VOLVER}"
							echo ""
							echo "${VERDE}iptables -P $chain $policy ${VOLVER}"
							echo ""
							echo "Desea crear la politica?"
							echo ""
							echo -e "SELECCIONE\n
							UNO '1' PARA CONFIRMAR
							CUALQUIER OTRA TECLA PARA CANCELAR"
							echo ""
							read -p "opcion " confirmar
							if [ $confirmar == 1 ]; then

								iptables -P $chain $policy
								echo "Politica creada correctamente"
							else 
								exit 0
							fi

						else if [ $opt_pol == 2 ]; then



							echo -e "Que cadena desea utilizar\n
							1. INPUT
							2. OUTPUT
							3. FORWARD"
							read opt_ch
							case $opt_ch in
								1) chain="INPUT" ;;
								2) chain="OUTPUT" ;;
								3) chain="FORWARD" ;;
								*) echo -e "Seleccion incorrecta!!!"
							esac
							 

							echo -e "Que Politica desea utilizar\n
							1. ACCEPT
							2. DROP"
							read opt_ch
							case $opt_ch in
								1) polit="ACCEPT" ;;
								2) polit="DROP" ;;
								*) echo -e "Seleccion incorrecta!!!"
							esac
							   
							###############Origen############
							echo -e "
							1. Firewall IP ORIGEN\n
							2. Firewall subnet de origen\n
							3. Firewall usar TODOS de origen\n
							"
							fe_origen=0
							read opt_ip_or
							case $opt_ip_or in
								1) echo -e "\nIngrese IP "
									read ip_source ;;
								2) echo -e "\nPor favor ingrese Subnet (e.g 192.168.10.0/24)"
									read ip_source ;;
								3) ip_source="0/0"
									fe_origen=1 ;;
								*) echo -e "Seleccion incorrecta!!!"
							esac 

							#########Getting Destination IP Address##########
							echo -e "
							1. Firewall IP DESTINO\n
							2. Firewall subnet de DESTINO\n
							3. Firewall usar TODOS de DESTINO\n
							"
							fe_destino=0
							read opt_ip_des
							case $opt_ip_des in
								1) echo -e "\nIngrese IP (192.168.2.2)"
									read ip_dest ;;
								2) echo -e "\nPor favor ingrese subnet de destino (e.g 192.168.10.0/24)"
									read ip_dest ;;
								3) ip_dest="0/0"
									fe_destino=1 ;;
								#4) ip_dest = "NULL" ;;
								*) echo -e "Seleccion incorrecta"
							esac
							###############Getting the Protocol#############
							echo -e "
							1. Sobre puerto especifico 
							2. Sin protocolo
							"
							read proto_ch
							case $proto_ch in
								1) echo -e "Ingrese el numero de puerto: "
									serv=0
									read proto ;;
								2) 
									echo "Seleccion:"
									echo ""
									echo -e "Todos los puertos"
									serv=1
									fe_destino=0
									fe_origen=0;;
								*) echo -e "Wrong option Selected!!!"
							esac



							#############Que hace la Regla?############# 
							read -p "Presione cualquier tecla para crear regla: " tecla
							echo -e "La regla a crear es: \n"
							if [ $serv == 1 ]; then
								#ufw deny from 11.12.13.0/24 to any port 80
								echo -e "\niptables -A $chain -s $ip_source -d $ip_dest -j $polit\n"
								gen=1
								echo -e "\n\tQuieres CREAR esta regla , Si=1 , No 2"
								read sino

								if [ $sino == 1 ]; then
									iptables -A $chain -s $ip_source -d $ip_dest -j $polit || echo "${ROJO}Fallo la ejecucion${VOLVER}"
									echo ""
									echo -e "La configuracion realizada fue la siguiente\n"
									echo ""
									echo "iptables -A $chain -s $ip_source -d $ip_dest -j $polit"
									echo ""
									read -p "presione cualquier tecla para continuar" tecla
									exit
								else if [ $sino == 2 ]; then
									clear
									echo "Go to main"
									exit
								fi
								fi



							else if [ $serv == 0 ]; then
								echo -e "La regla a crear es\n 
								"
								echo -e "\niptables -A $chain -s $ip_source -d $ip_dest -p $proto -j $polit\n"

								echo -e "\n\tQuieres CREAR esta regla , Si=1 , No 2"
								read sino

								if [ $sino == 1 ]; then
									iptables -A $chain -s $ip_source -d $ip_dest -p $proto -j $polit || echo "${ROJO}Fallo la ejecucion${VOLVER}"
									echo ""
									echo -e "La configuracion realizada fue la siguiente\n"
									echo ""
									echo "iptables -A $chain -s $ip_source -d $ip_dest -p $proto -j $polit"
									echo ""
									read -p "presione cualquier tecla para continuar" tecla
									exit

								else if [ $sino == 2 ]; then
									clear
									echo "Go to main"
									exit
								fi
								fi

							fi
							fi


						else if [ $opt_pol == 3 ]; then
							###############BAJA de POLITICA####################
							clear
							echo "${CYAN}##############Funcion de BAJA de POLITICAS ###########${VOLVER}"
							echo ""
							eliminacion=1
							while [ $eliminacion == 1 ]
							do
								echo "| ----------------------------------------------------------------|"
								echo "|          A continuacion podra observar una lista de politicas   |"
								echo "|             Seleccione el NRO de poltica que desea eliminar     |"
								echo "|                  Y la cadena a la que corresponde               |"
								echo "|                                                                 |"
								echo "|                   Ejemplo: iptables -D cadena NRO               |"
								echo "|                    Ejemplo: iptables -D INPUT 3                 |"
								echo "|---------------------------------------------------------------- |"
								echo ""
								echo ""
								read -p "Escriba cualquier tecla para empezar" tecla

								iptables -L --line-numbers
								echo ""

								echo -e "  De que cadena desea ELIMINAR\n
								1. INPUT
								2. OUTPUT
								3. FORWARD"
								read opt_ch
								case $opt_ch in
									1) cadena="INPUT" ;;
									2) cadena="OUTPUT" ;;
									3) cadena="FORWARD" ;;
									*) echo -e "Seleccion incorrecta!!!"
								esac

								read -p "Numero de politica a eliminar: " pol_num
								echo ""
								echo "La politica que va a eliminar es la siguiente"
								echo ""
								echo -e "iptables -D $cadena $pol_num"
								#echo -e "\niptables -A $chain -s $ip_source -d $ip_dest -p $proto -j $rule\n"
								echo ""
								echo -e "\n\tQuieres ELIMINAR la regla indicada arriba en el iptables? Si=1 , No=2"
								read sino

								if [ $sino == 1 ]; then
									echo "Ejecutando eliminacion"
									echo ""
									iptables -D $cadena $pol_num || echo "${ROJO}Fallo la ejecucion de la eliminacion${VOLVER}"
									echo -e "La configuracion realizada fue la siguiente\n"
									echo ""
									echo "${VERDE}iptables -D $cadena $pol_num ${VOLVER}"
									echo ""
									read -p "${BLANCO}presione cualquier tecla para volver a la lista${VOLVER}" tecla
									iptables -L --line-numbers

								else if [ $sino == 2 ]; then
									echo "Go to main"
									exit
								fi
								fi

								echo "Desea eliminar otra regla??"
								echo ""
								echo -e "Para eliminar otra regla escriba el NRO segun corresponda
								SI = 1          NO = 2 "
								read -p " " eliminacion
							done

						else 
							echo " Ingreso incorrecto "
							echo " Go to main "

						fi
						fi
	fi
}

FIREWALL_REPORT () {
	echo "${CYAN}#################Funcion de REPORTING FW ##################${VOLVER}"
	echo ""
	echo "| ----------------------------------------------------------------|"
	echo "|                             REPORTE                             |"
	echo "|                       FIREWALL DE IPTABLES                      |"
	echo "|---------------------------------------------------------------- |"
	echo ""
	echo ""
	FECHA=$(date +"%d-%m-%Y_%H:%M:%S")
	test -d Reportes_Firewall || mkdir Reportes_Firewall
	REP_FW=Reportes_Firewall/Reporte_$USER-IPTABLES-$FECHA.txt
	touch $REP_FW



	echo "GENERANDO REPORTE..."

	echo -ne "${VERDE}*****                     (33%)\r${VOLVER}"
	sleep 1
	echo -ne "${VERDE}*************             (66%)\r${VOLVER}"
	sleep 1
	echo -ne "${VERDE}***********************   (100%)\r${VOLVER}"
	echo -ne '\n'

	echo "###########################################################################" >> $REP_FW
	echo "" >> $REP_FW
	echo "##################### REPORTE DE IPTABLES ######################################" >> $REP_FW
	echo "" >> $REP_FW
	echo "***********************" >> $REP_FW
	echo "******* HOSTNAME ******" >> $REP_FW
	echo "***********************" >> $REP_FW
	echo "Hostname : $(hostname -s)" >> $REP_FW
	echo "" >> $REP_FW
	echo "***********************" >> $REP_FW
	echo "******* IPTABLES ******" >> $REP_FW
	echo "***********************" >> $REP_FW
	iptables -L --line-numbers >> $REP_FW
	echo "" >> $REP_FW
	echo "###########################################################################" >> $REP_FW
	sleep 2
	echo "El reporte ha sido generado en la carpeta\n
donde se ejecuto este script
El nombre del mismo es Reportes_Firewall"
sleep 3


}


Firewall () {
	echo "${CYAN}Bienvenido al MENU firewall iptables ${VOLVER}"
	echo ""
	echo "${CYAN}################################################################${VOLVER}" 
	echo "${CYAN}################### IPTABLES MENU ##############################${VOLVER}"
	echo "${CYAN}################################################################${VOLVER}" 
	echo ""
	OPT_FW=1
	while [ $OPT_FW != 4 ]
	do
		echo -e "/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\n" 
		#############IPTABLES  MAIN MENU ############ 
		echo -e "\t*****FIREWALL Menu*****\n
		1. Preparacion automatizada de firewall
		2. Customizacion de Firewall
		3. Reporte de estado y politicas de Firewall
		4. Salir"
		read OPT_FW
		case $OPT_FW in
			1) FIREWALL_PREP ;;
			2) FIREWALL_CUSTOM ;;
			3) FIREWALL_REPORT;;
			4) exit 0 ;;

			*) echo -e "Wrong option Selected!!!"
		esac
	done

}
main(){
	clear
	echo "${CYAN} BIENVENIDO AL MENU PRINCIPAL DEL SCRIPT DE${VOLVER}"
	echo ""
	echo "${CYAN}CONFIGURACION Y REPORTERIA DE VIRTUALES DEBIAN${VOLVER}"
	echo ""
	echo "${CYAN}################################################################${VOLVER}" 
	echo "${CYAN}####################### MENU PRINCIPAL #########################${VOLVER}"
	echo "${CYAN}################################################################${VOLVER}" 
	echo ""
	sleep 1
	ROOT_UID=0
	if [ $UID == $ROOT_UID ];
	then

		opt_main=1
		while [ $opt_main != 6 ]
		do
			echo -e "/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\n" 
			############# MAIN MENU ############ 
			echo -e "\t*****Main Menu*****\n
			1. Asignacion de Hostname
			2. Red
			3. Accesos
			4. Permisos
			5. Firewall
			6. Salir"
			read opt_main
			case $opt_main in
				1) Add_host ;;
				2) Red ;;
				3) Access_center ;;
				4) Permits ;;
				5) Firewall ;;
				6) exit 0 ;;

				*) echo -e "${ROJO}Wrong option Selected!!!${VOLVER}"
			esac
		done


	else
		echo -e "${ROJO}OPS! Tienes que ser root para continuar${VOLVER}"
	fi
}

echo "HOLA $USER, BIENVENIDO "
sleep 1
echo -ne "${BLANCO}Este script tiene como funcion la\r"
sleep 2
echo -ne "CONFIGURACION Y REPORTERIA DE VIRTUALES DEBIAN\r"
sleep 2
echo -ne "Presione cualquier tecla para comenzar a trabajar con el script....\r"
echo -ne '\n'
read -p "" tecla

main
exit 0



#!/bin/bash
############## CONFIGURACION Y REPORTERIA DE VIRTUALES DEBIAN ########
#
#ESTE SCRIPT TIENE COMO OBJETIVO OBTENER INFORMACION DE SO Y SU PREPARACION
echo "///////////////////////////////////////////////////////////////////"
echo "| ----------------------------------------------------------------|"
echo "|               *STATUS SO, CONFIGURATION AND REPORTING*          |"
echo "|               (Solo el usuario root, tiene permisos)            |"
echo "|---------------------------------------------------------------- |"

ROJO=$(tput setaf 1)
VERDE=$(tput setaf 2)
AMAR=$(tput setaf 3)
AZUL=$(tput setaf 4)
VIOLET=$(tput setaf 5)
CYAN=$(tput setaf 6)
BLANCO=$(tput setaf 7)
VOLVER=$(tput sgr0)



Add_host () {

	echo -e "${CYAN}CAMBIO DE HOSTNAME${VOLVER}"
	echo ""
	echo -e "ingrese nuevo nombre del host\n"
	read -p "Nuevo nombre de host: " nombrehost

	echo -e "El nombre de host ingresado es $nombrehost"

	echo -e "Desea configurar ese hostname\n 
	SI =1 NO= 2\n"

	read -p "ingrese opcion:" sino

	if [ $sino == 1 ]; then

		hostnamectl set-hostname $nombrehost

		echo -e "Nuevo hostname $nombrehost"

		echo -e "Recuerde que para que el este hostname quede aplicado se debe reiniciar el sistema"


	else if [ $sino == 2 ]; then
		clear
		echo "Go to main"

	fi
	fi

	exit 0

}


Red () {
	#USO DE VARIABLES ESPECIALES
	#INSTALACION DE PAQUETES NECESARIOS
	echo -e "${CYAN}Preparando informe de Red del SO${VOLVER}"
	echo ""
	sleep 1
	echo "Antes de comenzar precisamos instalar unas herramientas......"
	sleep 1
	dpkg -l | grep -v "^rc" | grep net-tools &> /dev/null
	if [ $? -ne 0 ]; then
		read -p " Usted no tiene netstat en su sistema el cual es necesario. Se instalara, presione enter para continuar..."
		apt-get update
		apt install net-tools

	else
			echo ""
			        echo "Ya tiene instalado net-tools"

	fi

	#awk '/enp0s8/{ print NR; exit }' /etc/network/interfaces

	#echo "Hostname : $(hostname -s)"
	#echo "Dominio DNS : $(cat /etc/resolv.conf | grep 'nameserver')"
	FECHA=$(date +"%d-%m-%Y_%H:%M:%S")
	test -d Reportes_red || mkdir Reportes_red
	REP_RED=Reportes_red/Reporte_$USER-$FECHA.txt
	touch $REP_RED



	echo "GENERANDO REPORTE..."

	echo -ne "${VERDE} *****                     (33%)\r${VOLVER}"
	sleep 1
	echo -ne "${VERDE}*************             (66%)\r${VOLVER}"
	sleep 1
	echo -ne "${VERDE}***********************   (100%)\r${VOLVER}"
	echo -ne '\n'

	echo "###########################################################################" >> $REP_RED
	echo "" >> $REP_RED
	echo "##################### REPORTE DE RED ######################################" >> $REP_RED
	echo "" >> $REP_RED
	echo "***********************" >> $REP_RED
	echo "******* HOSTNAME ******" >> $REP_RED
	echo "***********************" >> $REP_RED
	echo "Hostname : $(hostname -s)" >> $REP_RED
	echo "" >> $REP_RED
	echo "***********************" >> $REP_RED
	echo "******* Dominio *******" >> $REP_RED
	echo "***********************" >> $REP_RED
	echo "Dominio DNS : $(cat /etc/resolv.conf | grep 'nameserver')" >> $REP_RED
	echo "" >> $REP_RED
	echo "***********************" >> $REP_RED
	echo "***Enrutamiento  Red***" >> $REP_RED
	echo "***********************" >> $REP_RED
	netstat -nr >> $REP_RED
	echo "" >> $REP_RED
	echo "**************************************" >> $REP_RED 
	echo "**Informacion Trafico de la Interfaz**" >> $REP_RED
	echo "**************************************" >> $REP_RED
	netstat -i >> $REP_RED
	echo "" >> $REP_RED
	echo "**************************************" >> $REP_RED 
	echo "**Conexiones establecidas**" >> $REP_RED
	echo "**************************************" >> $REP_RED
	netstat -natu | grep 'ESTABLISHED' >> $REP_RED
	echo "" >> $REP_RED
	echo "**************************************" >> $REP_RED 
	echo "**Listening connections**" >> $REP_RED
	echo "**************************************" >> $REP_RED
	netstat -an | grep 'LISTEN' >> $REP_RED
	echo "###########################################################################" >> $REP_RED
	sleep 2
	echo "El reporte ha sido generado en la carpeta\n
donde se ejecuto este script"
sleep 3
}


USR_ADD () {

	echo -e "${CYAN}AGREGAR USUARIOS${VOLVER}"
	echo ""
	#USO DE ARRAY
	array_user=()
	opt_usr=1
	while [ $opt_usr != 2 ]
	do
		read -p "ingrese nombre de usuario  " usr
		sleep 1
		echo "Agregando usuario"
		useradd -s /bin/bash -d /home/$usr -m $usr                   
		echo " Generado Usuario: $usr"                              
		echo "Ingrese Contraseña o cambie contraseña de este usuario"   
		echo ""
		passwd $usr
		echo ""
		echo "Usuario agregado $usr"
		echo ""
		array_usr+=($usr)
		echo "USUARIOS GENERADOS: ${array_usr[@]}"
		echo ${array_usr[1]}
		echo ${array_usr[0]}
		echo ""
		echo ""
		echo -e "Desea agregar otro usuario\n
		SI= 1
		NO= 2"
		echo ""
		read -p "ingrese el numero que corresponda: " sino
		if [ $sino == 1 ]; then
			opt_usr=1

		else if [ $sino == 2 ]; then
			opt_usr=2

		else
			exit
		fi
		fi
	done

	#NOTAS DE APRENDIZAJE
	#useradd -s /bin/bash -d /home/$USRNAME -m $USRNAME
	#echo $contrasena | passwd --stdin $NEWUSER >& /dev/null
	#useradd -s /bin/bash -d /home/$NEWUSER -m $NEWUSER >& /dev/null ;  echo -e "$PASSWORD\n$PASSWORD" >& /dev/null |  passwd $NEWUSER >& /dev/null
	#useradd -s /bin/bash -d /home/fede -m fede >& /dev/null 
	#echo "password" | passwd --stdin "fede" >& /dev/null
	#useradd -s /bin/bash -d /home/vale -m vale >& /dev/null ;  echo -e "pepe\npepe" >& /dev/null |  passwd vale >& /dev/null
	#TIP para leer status contrasenas
	#passwd -S javier
	#javier P 08/16/2012 0 30 5 -1
	#Las opciones que tienen las contraseñas son:
	#L  -> Contraseña bloqueada
	#NP -> Sin contraseña
	#P  -> Contraseña válida
}


USR_DEL () {
	echo -e "${CYAN}ELIMINACION DE USUARIOS${VOLVER}"
	echo ""
	array_user=()
	opt_usr=1
	while [ $opt_usr != 2 ]
	do	
		echo ""
		read -p "Que usuario va a eliminar? " usrdel
		echo ""
		echo "Eliminando usuario..."
		sleep 1
		userdel $usrdel
		if [ $? -ne 0 ]; then
			echo ""
			echo "${ROJO}El usuario indicado no existe${VOLVER}"
			echo ""
		else
			echo -e "\n
			Usuario eliminado con exito"	
			array_usr+=($usrdel)
		fi
		echo -e "\n
		Desea ELIMINAR otro usuario\n
		SI= 1
		NO= 2"
		read -p "ingrese si o no segun corresponda " sino
		if [ $sino == 1 ]; then
			opt_usr=1

		else if [ $sino == 2 ]; then
			opt_usr=2
			echo -e "Total de usuarios eliminados> ${array_usr[@]} "

		else
			exit
		fi
		fi
	done
}


REP_USR() {
	echo -e "${CYAN}REPORTE DE USUARIOS${VOLVER}"
	echo ""
	HOST=$(hostname)
	FECHA=$(date +"%d-%m-%Y_%H:%M:%S")
	test -d Reportes_user || mkdir Reportes_user
	REP_USERS=Reportes_user/Reporte_$HOST-$FECHA.txt
	touch $REP_USERS



	echo "${CYAN}COMENZANDO REPORTE...${VOLVER}"

	echo -ne "${VERDE}*****                     (33%)\r${VOLVER}"
	sleep 1
	echo -ne "${VERDE}*************             (66%)\r${VOLVER}"
	sleep 1
	echo -ne "${VERDE}***********************   (100%)\r${VOLVER}"
	echo -ne '\n'

	echo "################################################################################" >> $REP_USERS
	echo "" >> $REP_USERS
	echo "##################### REPORTE DE USUARIOS ######################################" >> $REP_USERS
	echo "" >> $REP_USERS
	echo "***********************" >> $REP_USERS
	echo "******* HOSTNAME ******" >> $REP_USERS
	echo "***********************" >> $REP_USERS
	echo "Hostname : $(hostname -s)" >> $REP_USERS
	echo "" >> $REP_USERS
	echo "**********************************" >> $REP_USERS
	echo "******* Cantidad de usuarios *****" >> $REP_USERS
	echo "**********************************" >> $REP_USERS
	echo "" >> $REP_USERS
	cut -d: -f1 /etc/passwd | wc -l >> $REP_USERS
	#fila 1 del /etc/passwd, y contamos
	echo "" >> $REP_USERS
	echo "****************************************" >> $REP_USERS
	echo "********** Listado de usuarios *********" >> $REP_USERS
	echo "********* con acceso /bin/bash *********" >> $REP_USERS
	echo "****************************************" >> $REP_USERS
	echo "" >> $REP_USERS
	#lISTADO DE USUARIOS
	cat /etc/passwd | grep '/bin/bash' | cut -d":" -f1 >> $REP_USERS
	echo "" >> $REP_USERS
	echo "**********************************" >> $REP_USERS
	echo "******* Listado de usuarios ******" >> $REP_USERS
	echo "********* Sin contraseña *********" >> $REP_USERS
	echo "**********************************" >> $REP_USERS
	echo "" >> $REP_USERS
	getent shadow | grep -Po '^[^:]*(?=:.?:)' >> $REP_USERS
	echo "" >> $REP_USERS
	echo "********************************************" >> $REP_USERS
	echo "********* Ultimas conexion de cada **********" >> $REP_USERS
	echo "********* usuario en el ultimo mes *********" >> $REP_USERS
	echo "********************************************" >> $REP_USERS
	echo "" >> $REP_USERS
	last -s '-1 month'  | sort -u -k1,1 >> $REP_USERS
	echo "" >> $REP_USERS


	echo "${BLANCO}Obteniendo informacion de sistema${VOLVER}"
	sleep 1 
	echo -ne "${VERDE}*************             (66%)\r${VOLVER}"
	sleep 1
	echo -ne "${VERDE}***********************   (99%)\r${VOLVER}"
	sleep 1
	echo -ne "${VERDE}***********************   (100%)\r${VOLVER}"
	echo -ne '\n'
	echo "${VERDE}REPORTE FINALIZADO${VOLVER}"
	echo -e "
	Puede visualizar el mismo
	en la carpeta Reportes_user"
	sleep 1

}


Access_center () {

	echo "${CYAN}################################################################${VOLVER}" 
	echo "${CYAN}################### Portal de accesos ##########################${VOLVER}"
	echo "${CYAN}################################################################${VOLVER}" 
	echo ""

	opt_portal=1
	while [ $opt_portal != 4 ]
	do
		echo -e "/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\n" 
		############# Portal MENU ############ 
		echo -e "\t*****PORTAL USER & GROUP MENU*****\n
		1. Creacion de usuarios
		2. Eliminacion de usuarios
		3. Reporte de usuarios
		4. Salir"
		read opt_portal
		case $opt_portal in
			1) USR_ADD ;;
			2) USR_DEL ;;
			3) REP_USR ;;
			4) exit 0 ;;

			*) echo -e "Wrong option Selected!!!"
		esac
	done

}


PER_ADD () {
		
	sino=1
	while [ $sino != 2 ]
	do
		echo "${CYAN}Funcion de agregacion de permisos${VOLVER}"
		echo ""
		echo -e "
		Sobre que archivo o directorio desea agregar permisos?

		Favor detallar ruta completa

		Ej /home/$USER/trabajos/archivo.txt"
		echo ""
		read -p "Archivo a modificar permisos " file
		echo ""
		echo "Procesando...."
		sleep 1
		echo ""
		clear
		echo -e "
		${BLANCO}Ejemplo de asignacion de permisos

		0 = --- = sin acceso
		1 = --x = ejecución
		2 = -w- = escritura
		3 = -wx = escritura y ejecución
		4 = r-- = lectura
		5 = r-x = lectura y ejecución
		6 = rw- = lectura y escritura
		7 = rwx = lectura, escritura y ejecución

		Permiso ejemplo> -rw-rw-r--  1 lorenzo lorenzo  11K dic  2 19:06 feed.log

		El primero grupo de izquierda a derecha pertenece al propietario
		El segundo grupo al Grupo Propietario
		El tercer grupo a los demas

		400 Lectura solo al propietario
		040 Lectura solo al grupo
		004 Lectura solo a los demas

		${VOLVER}"
		echo "Que permisos desea asignarle a $file ?"
		echo ""
		echo "Escribir solo los numeros de Mascara. Ej 666"
		echo ""
		read -p "" mask
		echo -e "

		"
		chmod $mask $file || echo "${ROJO}Fallo la ejecucion de Permisos${VOLVER}"
		echo ""
		echo -e "La configuracion realizada fue la siguiente\n

		chomod $mask $file 

		"
		echo "${VIOLET}Desea Cambiar otro permiso?${VOLVER}"
		echo -e "Seleccione
		SI= 1
		NO= 2
		"
		read -p "Ingrese su opcion " sino
		#sino=$valor
	done

}

PROP_ADD ()
{
	#ROJO=$(tput setaf 1)
	#VERDE=$(tput setaf 2)
	#AMAR=$(tput setaf 3)
	#AZUL=$(tput setaf 4)
	#VIOLET=$(tput setaf 5)
	#CYAN=$(tput setaf 6)
	#BLANCO=$(tput setaf 7)
	#VOLVER=$(tput sgr0)

	folder=()
	opt_prop=1
	while [ $opt_prop != 2 ]
	do
		echo "${CYAN}##############Funcion de definicion de PROPIETARIO ###########${VOLVER}"
		echo ""
		echo -e "
		Esta herramienta tiene como funcion gestionar el propietario 
		de un archivo o directorio"
		echo ""
		echo -e "
		Indique el nombre de usuario del nuevo propietario\n
		Ej $USER"
		echo ""
		read -p "${BLANCO}Nombre de usuario${VOLVER} " usuario 
		echo ""
		bucle=1
		while [ $bucle != 2 ]
		do
			echo -e "
			DETALLE EL ARCHIVO O DIRECTORIO QUE LE QUIERE ASIGNAR AL PROPIETARIO SELECCIONADO

			Favor detallar ruta completa en caso de estar en una carpeta diferente 
			a la que usted esta situado.

			Ej /home/$USER/trabajos/archivo.txt

			"	
			read -p "${BLANCO}Ruta o nombre${VOLVER}  " ruta
			echo ""
			folder+=($ruta)

			echo "Desea agregar otra ruta mas para este propietario? "
			echo ""
			echo -e "
			Seleccione:
			Si = 1       No =2"
			read -p "${BLANCO}Seleccion>  ${VOLVER}" bucle
		done
		echo ""
		echo "${VERDE}Procesando Solicitud${VOLVER}"
		sleep 1
		echo ""
		echo -e "
		EJECUTANDO  comando

		chown $usuario ${folder[@]} 

		"

		chown $usuario ${folder[@]} || echo "${ROJO}Fallo la ejecucion de PROPIETARIO\n Revise que los datos ingresados sean correctos\n segun los ejemplos dados ${VOLVER}"

		sleep 1 
		echo -ne "${VERDE}*****                     (25%)\r${VOLVER}"
		sleep 1
		echo -ne "${VERDE}*************             (60%)\r${VOLVER}"
		sleep 1
		echo -ne "${VERDE}***********************   (100%)\r${VOLVER}"
		echo -ne '\n'
		echo "${VERDE}CAMBIO DE PROPIETARIO FINALIZADO${VOLVER}"
		echo -e "

		"
		echo "Desea realizar otro cambio?"
		echo -e "
		Seleccione:
		Si = 1       No =2"
		read -p "${BLANCO}Seleccion>  ${VOLVER}" opt_prop
		folder=()
	done

}


Permits () {

	echo "${CYAN}Bienvenido a la asignacion de permisos${VOLVER}"
	echo ""
	echo "${CYAN}################################################################${VOLVER}" 
	echo "${CYAN}################### Portal de PERMISOS #########################${VOLVER}"
	echo "${CYAN}################################################################${VOLVER}" 
	echo ""

	opt_per=1
	while [ $opt_per != 3 ]
	do
		echo -e "/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\n" 
		############# PERMISOS MENU ############ 
		echo -e "\t*****PERMISOS Menu*****\n
		1. Permisos a directorios y archivos
		2. Propietarios
		3. Salir"
		read opt_per
		case $opt_per in
			1) PER_ADD ;;
			2) PROP_ADD ;;
			3) exit 0 ;;

			*) echo -e "Wrong option Selected!!!"
		esac
	done
	#chown user02 file1
	#chown :groupA file1
	#chown user02:groupA file2
}

FIREWALL_PREP () {
	echo "${CYAN} Preparacion de firewall ${VOLVER}"
	echo ""
	echo ""

	#Habilitacion de conexion
	iptables -P INPUT ACCEPT              
	iptables -P FORWARD ACCEPT
	iptables -P OUTPUT ACCEPT
	# Flush all current rules from iptables
	#
	iptables -F
	iptables -t nat -F
	clear
	sleep 1 
	echo "${BLANCO}Configurando puertos permitidos${VOLVER}"
	echo -ne "${VERDE}*****                     (33%)\r${VOLVER}"
	sleep 1
	echo -ne "${VERDE}*************             (66%)\r${VOLVER}"
	sleep 1
	echo -ne "${VERDE}***********************   (100%)\r${VOLVER}"
	echo -ne '\n'
	echo ""
	echo "${BLANCO}Configurando conexiones establecidas${VOLVER}"
	echo ""
	echo -ne "${VERDE}*****                     (33%)\r${VOLVER}"
	sleep 1
	echo -ne "${VERDE}*************             (66%)\r${VOLVER}"
	sleep 1
	echo -ne "${VERDE}***********************   (100%)\r${VOLVER}"
	echo -ne '\n'
	echo ""
	echo "${BLANCO}Configurando politicas por default${VOLVER}"
	echo ""
	echo ""
	echo -ne "${VERDE}*****                     (33%)\r${VOLVER}"
	sleep 1
	echo -ne "${VERDE}*************             (66%)\r${VOLVER}"
	sleep 1
	echo -ne "${VERDE}***********************   (100%)\r${VOLVER}"
	echo -ne '\n'
	echo ""
	echo "${VERDE}
	CONFIGURACION DE FIREWALL FINALIZADA
	${VOLVER}"
	sleep 3
	echo ""
	#PUERTOS PERMITIDOS 
	# ICMP (Ping)
	iptables -t filter -A INPUT -p icmp -j ACCEPT
	iptables -t filter -A OUTPUT -p icmp -j ACCEPT
	# SSH
	iptables -t filter -A INPUT -p tcp --dport 22 -j ACCEPT
	iptables -t filter -A OUTPUT -p tcp --dport 22 -j ACCEPT
	iptables -t filter -A FORWARD -p tcp --dport 22 -j ACCEPT
	# Git
	iptables -t filter -A OUTPUT -p tcp --dport 9418 -j ACCEPT
	iptables -t filter -A INPUT -p tcp --dport 9418 -j ACCEPT
	# DNS
	iptables -t filter -A OUTPUT -p tcp --dport 53 -j ACCEPT
	iptables -t filter -A OUTPUT -p udp --dport 53 -j ACCEPT
	iptables -t filter -A INPUT -p tcp --dport 53 -j ACCEPT
	iptables -t filter -A INPUT -p udp --dport 53 -j ACCEPT
	# PROTOCOLOS DE MAIL
	iptables -t filter -A INPUT -p tcp --dport 25 -j ACCEPT
	iptables -t filter -A OUTPUT -p tcp --dport 25 -j ACCEPT
	iptables -t filter -A INPUT -p tcp --dport 110 -j ACCEPT
	iptables -t filter -A OUTPUT -p tcp --dport 110 -j ACCEPT
	iptables -t filter -A INPUT -p tcp --dport 143 -j ACCEPT
	iptables -t filter -A OUTPUT -p tcp --dport 143 -j ACCEPT
	#CONFIGURACION DE HORARIO VIA SERVER NTP
	iptables -t filter -A OUTPUT -p udp --dport 123 -j ACCEPT


	# ACCEPT DE LOCALHOST 
	iptables -A INPUT -i lo -j ACCEPT



	# AUTORIZAMOS CONEXIONES ESTABLECIDAS 
	iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
	iptables -A FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT
	iptables -A OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
	iptables -t filter -A INPUT -i lo -j ACCEPT
	iptables -t filter -A OUTPUT -o lo -j ACCEPT

	# POLITICAS POR DEFECTO EN DROP
	#
	iptables -P INPUT DROP              
	iptables -P FORWARD DROP
	iptables -P OUTPUT DROP

	echo "Mostrando configuracion en pantalla :"
	echo ""
	# Comprobamos cómo quedan las reglas
	iptables -L -n

}



FIREWALL_CUSTOM () {

	echo "${CYAN} Bienvenido al creador de politicas CUSTOM${VOLVER}"
	echo ""
	echo " Desea modificar las politicas por defecto, especificar una politca personalizada, o eliminar? "
	echo ""
	echo -e "${BLANCO}
	Seleccione segun corresponda:
	Defecto = 1
	Especificar = 2 
	ELIMINAR = 3 ${VOLVER}"
	echo ""
	read -p "" opt_pol 

	if [ $opt_pol == 1 ]; then 

		echo ""
		echo "Creandor de reglas Default"
		echo ""
		echo -e "${BLANCO}Que cadena desea utilizar\n
		  1. INPUT
		    2. OUTPUT
		      3. Forward${VOLVER}"
		        read -p " " opt_ch
			  case $opt_ch in
				     1) chain="INPUT" ;;
				        2) chain="OUTPUT" ;;
					   3) chain="FORWARD" ;;
					      *) echo -e "Seleccion incorrecta!!!"
						        esac
							echo ""
							echo ""
							echo "A continuacion...."
							echo -e "${BLANCO}Que cadena desea utilizar\n
							1. ACCEPT
							2. DROP${VOLVER}"
							read -p " " opt_policy
							case $opt_policy in
								1) policy="ACCEPT" ;;
								2) policy="DROP" ;;
								*) echo -e "Seleccion incorrecta!!!"
							esac
							echo ""
							echo ""
							echo "${AMAR}La politica a crear es la siguiente:${VOLVER}"
							echo ""
							echo "${VERDE}iptables -P $chain $policy ${VOLVER}"
							echo ""
							echo "Desea crear la politica?"
							echo ""
							echo -e "SELECCIONE\n
							UNO '1' PARA CONFIRMAR
							CUALQUIER OTRA TECLA PARA CANCELAR"
							echo ""
							read -p "opcion " confirmar
							if [ $confirmar == 1 ]; then

								iptables -P $chain $policy
								echo "Politica creada correctamente"
							else 
								exit 0
							fi

						else if [ $opt_pol == 2 ]; then



							echo -e "Que cadena desea utilizar\n
							1. INPUT
							2. OUTPUT
							3. FORWARD"
							read opt_ch
							case $opt_ch in
								1) chain="INPUT" ;;
								2) chain="OUTPUT" ;;
								3) chain="FORWARD" ;;
								*) echo -e "Seleccion incorrecta!!!"
							esac
							 

							echo -e "Que Politica desea utilizar\n
							1. ACCEPT
							2. DROP"
							read opt_ch
							case $opt_ch in
								1) polit="ACCEPT" ;;
								2) polit="DROP" ;;
								*) echo -e "Seleccion incorrecta!!!"
							esac
							   
							###############Origen############
							echo -e "
							1. Firewall IP ORIGEN\n
							2. Firewall subnet de origen\n
							3. Firewall usar TODOS de origen\n
							"
							fe_origen=0
							read opt_ip_or
							case $opt_ip_or in
								1) echo -e "\nIngrese IP "
									read ip_source ;;
								2) echo -e "\nPor favor ingrese Subnet (e.g 192.168.10.0/24)"
									read ip_source ;;
								3) ip_source="0/0"
									fe_origen=1 ;;
								*) echo -e "Seleccion incorrecta!!!"
							esac 

							#########Getting Destination IP Address##########
							echo -e "
							1. Firewall IP DESTINO\n
							2. Firewall subnet de DESTINO\n
							3. Firewall usar TODOS de DESTINO\n
							"
							fe_destino=0
							read opt_ip_des
							case $opt_ip_des in
								1) echo -e "\nIngrese IP (192.168.2.2)"
									read ip_dest ;;
								2) echo -e "\nPor favor ingrese subnet de destino (e.g 192.168.10.0/24)"
									read ip_dest ;;
								3) ip_dest="0/0"
									fe_destino=1 ;;
								#4) ip_dest = "NULL" ;;
								*) echo -e "Seleccion incorrecta"
							esac
							###############Getting the Protocol#############
							echo -e "
							1. Sobre puerto especifico 
							2. Sin protocolo
							"
							read proto_ch
							case $proto_ch in
								1) echo -e "Ingrese el numero de puerto: "
									serv=0
									read proto ;;
								2) 
									echo "Seleccion:"
									echo ""
									echo -e "Todos los puertos"
									serv=1
									fe_destino=0
									fe_origen=0;;
								*) echo -e "Wrong option Selected!!!"
							esac



							#############Que hace la Regla?############# 
							read -p "Presione cualquier tecla para crear regla: " tecla
							echo -e "La regla a crear es: \n"
							if [ $serv == 1 ]; then
								#ufw deny from 11.12.13.0/24 to any port 80
								echo -e "\niptables -A $chain -s $ip_source -d $ip_dest -j $polit\n"
								gen=1
								echo -e "\n\tQuieres CREAR esta regla , Si=1 , No 2"
								read sino

								if [ $sino == 1 ]; then
									iptables -A $chain -s $ip_source -d $ip_dest -j $polit || echo "${ROJO}Fallo la ejecucion${VOLVER}"
									echo ""
									echo -e "La configuracion realizada fue la siguiente\n"
									echo ""
									echo "iptables -A $chain -s $ip_source -d $ip_dest -j $polit"
									echo ""
									read -p "presione cualquier tecla para continuar" tecla
									exit
								else if [ $sino == 2 ]; then
									clear
									echo "Go to main"
									exit
								fi
								fi



							else if [ $serv == 0 ]; then
								echo -e "La regla a crear es\n 
								"
								echo -e "\niptables -A $chain -s $ip_source -d $ip_dest -p $proto -j $polit\n"

								echo -e "\n\tQuieres CREAR esta regla , Si=1 , No 2"
								read sino

								if [ $sino == 1 ]; then
									iptables -A $chain -s $ip_source -d $ip_dest -p $proto -j $polit || echo "${ROJO}Fallo la ejecucion${VOLVER}"
									echo ""
									echo -e "La configuracion realizada fue la siguiente\n"
									echo ""
									echo "iptables -A $chain -s $ip_source -d $ip_dest -p $proto -j $polit"
									echo ""
									read -p "presione cualquier tecla para continuar" tecla
									exit

								else if [ $sino == 2 ]; then
									clear
									echo "Go to main"
									exit
								fi
								fi

							fi
							fi


						else if [ $opt_pol == 3 ]; then
							###############BAJA de POLITICA####################
							clear
							echo "${CYAN}##############Funcion de BAJA de POLITICAS ###########${VOLVER}"
							echo ""
							eliminacion=1
							while [ $eliminacion == 1 ]
							do
								echo "| ----------------------------------------------------------------|"
								echo "|          A continuacion podra observar una lista de politicas   |"
								echo "|             Seleccione el NRO de poltica que desea eliminar     |"
								echo "|                  Y la cadena a la que corresponde               |"
								echo "|                                                                 |"
								echo "|                   Ejemplo: iptables -D cadena NRO               |"
								echo "|                    Ejemplo: iptables -D INPUT 3                 |"
								echo "|---------------------------------------------------------------- |"
								echo ""
								echo ""
								read -p "Escriba cualquier tecla para empezar" tecla

								iptables -L --line-numbers
								echo ""

								echo -e "  De que cadena desea ELIMINAR\n
								1. INPUT
								2. OUTPUT
								3. FORWARD"
								read opt_ch
								case $opt_ch in
									1) cadena="INPUT" ;;
									2) cadena="OUTPUT" ;;
									3) cadena="FORWARD" ;;
									*) echo -e "Seleccion incorrecta!!!"
								esac

								read -p "Numero de politica a eliminar: " pol_num
								echo ""
								echo "La politica que va a eliminar es la siguiente"
								echo ""
								echo -e "iptables -D $cadena $pol_num"
								#echo -e "\niptables -A $chain -s $ip_source -d $ip_dest -p $proto -j $rule\n"
								echo ""
								echo -e "\n\tQuieres ELIMINAR la regla indicada arriba en el iptables? Si=1 , No=2"
								read sino

								if [ $sino == 1 ]; then
									echo "Ejecutando eliminacion"
									echo ""
									iptables -D $cadena $pol_num || echo "${ROJO}Fallo la ejecucion de la eliminacion${VOLVER}"
									echo -e "La configuracion realizada fue la siguiente\n"
									echo ""
									echo "${VERDE}iptables -D $cadena $pol_num ${VOLVER}"
									echo ""
									read -p "${BLANCO}presione cualquier tecla para volver a la lista${VOLVER}" tecla
									iptables -L --line-numbers

								else if [ $sino == 2 ]; then
									echo "Go to main"
									exit
								fi
								fi

								echo "Desea eliminar otra regla??"
								echo ""
								echo -e "Para eliminar otra regla escriba el NRO segun corresponda
								SI = 1          NO = 2 "
								read -p " " eliminacion
							done

						else 
							echo " Ingreso incorrecto "
							echo " Go to main "

						fi
						fi
	fi
}

FIREWALL_REPORT () {
	echo "${CYAN}#################Funcion de REPORTING FW ##################${VOLVER}"
	echo ""
	echo "| ----------------------------------------------------------------|"
	echo "|                             REPORTE                             |"
	echo "|                       FIREWALL DE IPTABLES                      |"
	echo "|---------------------------------------------------------------- |"
	echo ""
	echo ""
	FECHA=$(date +"%d-%m-%Y_%H:%M:%S")
	test -d Reportes_Firewall || mkdir Reportes_Firewall
	REP_FW=Reportes_Firewall/Reporte_$USER-IPTABLES-$FECHA.txt
	touch $REP_FW



	echo "GENERANDO REPORTE..."

	echo -ne "${VERDE}*****                     (33%)\r${VOLVER}"
	sleep 1
	echo -ne "${VERDE}*************             (66%)\r${VOLVER}"
	sleep 1
	echo -ne "${VERDE}***********************   (100%)\r${VOLVER}"
	echo -ne '\n'

	echo "###########################################################################" >> $REP_FW
	echo "" >> $REP_FW
	echo "##################### REPORTE DE IPTABLES ######################################" >> $REP_FW
	echo "" >> $REP_FW
	echo "***********************" >> $REP_FW
	echo "******* HOSTNAME ******" >> $REP_FW
	echo "***********************" >> $REP_FW
	echo "Hostname : $(hostname -s)" >> $REP_FW
	echo "" >> $REP_FW
	echo "***********************" >> $REP_FW
	echo "******* IPTABLES ******" >> $REP_FW
	echo "***********************" >> $REP_FW
	iptables -L --line-numbers >> $REP_FW
	echo "" >> $REP_FW
	echo "###########################################################################" >> $REP_FW
	sleep 2
	echo "El reporte ha sido generado en la carpeta\n
donde se ejecuto este script
El nombre del mismo es Reportes_Firewall"
sleep 3


}


Firewall () {
	echo "${CYAN}Bienvenido al MENU firewall iptables ${VOLVER}"
	echo ""
	echo "${CYAN}################################################################${VOLVER}" 
	echo "${CYAN}################### IPTABLES MENU ##############################${VOLVER}"
	echo "${CYAN}################################################################${VOLVER}" 
	echo ""
	OPT_FW=1
	while [ $OPT_FW != 4 ]
	do
		echo -e "/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\n" 
		#############IPTABLES  MAIN MENU ############ 
		echo -e "\t*****FIREWALL Menu*****\n
		1. Preparacion automatizada de firewall
		2. Customizacion de Firewall
		3. Reporte de estado y politicas de Firewall
		4. Salir"
		read OPT_FW
		case $OPT_FW in
			1) FIREWALL_PREP ;;
			2) FIREWALL_CUSTOM ;;
			3) FIREWALL_REPORT;;
			4) exit 0 ;;

			*) echo -e "Wrong option Selected!!!"
		esac
	done

}
main(){
	clear
	echo "${CYAN} BIENVENIDO AL MENU PRINCIPAL DEL SCRIPT DE${VOLVER}"
	echo ""
	echo "${CYAN}CONFIGURACION Y REPORTERIA DE VIRTUALES DEBIAN${VOLVER}"
	echo ""
	echo "${CYAN}################################################################${VOLVER}" 
	echo "${CYAN}####################### MENU PRINCIPAL #########################${VOLVER}"
	echo "${CYAN}################################################################${VOLVER}" 
	echo ""
	sleep 1
	ROOT_UID=0
	if [ $UID == $ROOT_UID ];
	then

		opt_main=1
		while [ $opt_main != 6 ]
		do
			echo -e "/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\n" 
			############# MAIN MENU ############ 
			echo -e "\t*****Main Menu*****\n
			1. Asignacion de Hostname
			2. Red
			3. Accesos
			4. Permisos
			5. Firewall
			6. Salir"
			read opt_main
			case $opt_main in
				1) Add_host ;;
				2) Red ;;
				3) Access_center ;;
				4) Permits ;;
				5) Firewall ;;
				6) exit 0 ;;

				*) echo -e "${ROJO}Wrong option Selected!!!${VOLVER}"
			esac
		done


	else
		echo -e "${ROJO}OPS! Tienes que ser root para continuar${VOLVER}"
	fi
}

echo "HOLA $USER, BIENVENIDO "
sleep 1
echo -ne "${BLANCO}Este script tiene como funcion la\r"
sleep 2
echo -ne "CONFIGURACION Y REPORTERIA DE VIRTUALES DEBIAN\r"
sleep 2
echo -ne "Presione cualquier tecla para comenzar a trabajar con el script....\r"
echo -ne '\n'
read -p "" tecla

main
exit 0




