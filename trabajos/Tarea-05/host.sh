echo -e "ingrese nuevo nombre del host\n"
read -p "Nuevo nombre de host: " nombrehost

echo -e "El nombre de host ingresado es $nombrehost"

echo -e "Desea configurar ese hostname\n 
SI =1 NO= 2\n"

read -p "Opcion:" yesno

echo "la opcion elegida es $yesno"

if [ $yesno == 1 ]; then

	hostname $nombrehost
	echo -e "Nuevo hostname $nombrehost"

else if [ $yesno == 2 ]; then

	clear
	echo "Go to main"

else 
	clear
	echo "ingreso incorrecto\n 
	vuelve al Menu principal"

fi
fi

exit 0
