#!/bin/bash
##############UFW Administracion servidor########
echo -e "****************Welcome*************"

#Administracion servidor APACHE
#



change_ip() {

	echo -e "#### ATENCION ###\n
	PARA QUE ESTA CONFIGURACION SE REALICE EN EL EQUIPO
	DEBE REALIZAR UN REBOOT POSTERIORMENTE
	A LA EJECUCION DE ESTE SCRIPT"
	echo -e "Presione cualquier tecla para continuar"
	read -p "" tecla
	clear
	echo "Esta es la actual configuracion de las interfaces:\n

	"
	cat /etc/network/interfaces


	echo -e "\nSe encuentra configurado actualmente como DHCP?\n
	SI =1 NO= 2\n"

	read -p "Opcion: " yesno


	if [ $yesno == 1 ]; then
		echo -e "configuramos como estatica antes de configurar ip.\n"

		echo -e "Ingrese la nueva IP estatica\n"
		read -p "Nueva IP: " newip

		echo -e "ingrese mascara de red\n"
		read -p "Mascara de red " maskip

		echo -e "ingrese puerta de enlace predeterminada\n"
		read -p "Puerta de enlace " gatewayip

		echo -e "ingrese interfaz de red"
		read -p "Interfaz de red (Ej enp0s8): " interfazred

		echo -e "\n
		auto $interfazred
		iface $interfazred inet static
		address $newip
		netmask $maskip
		gateway $gatewayip
		 " >> /etc/network/interfaces

		 echo -e "Quedo configurado de la siguiente manera\n"
		 cat /etc/network/interfaces


	 else if [ $yesno == 2 ]; then

		 echo -e "configuramos ip nueva\n"

		 echo -e "Ingrese la ip actual\n"
		 read -p "Ip Anterior: " oldip

		 echo -e "Ingrese la nueva IP "
		 read -p "Nueva IP: " newip

		 sed -i "s/$oldip/$newip/g" /etc/network/interfaces

		 echo -e "Quedo configurado de la siguiente manera\n"
		 cat /etc/network/interfaces

	 fi
	fi

	exit 0

}

hostname() {

	bash host.sh

}


Stack_install() {

	opt_stack=0
	while [ $opt_stack == 0 ]
	do
		echo -e "Seleccione el componente de stack a instalar\n 
		1= Apache 
		2= Apache y PHP 
		3= Php y MySql"

		read -p "Ingrese su opcion: " opt_stack
		if [ $opt_stack == 1 ]; then

			#Instalar Apache
			apt update
			apt-get install apache2



		else if [ $opt_stack == 2  ]; then

			#instalar Apache
			apt update
			apt-get install apache2
			#Instalar php
			apt install php libapache2-mod-php php-mysql

			#Instalar MYSQL
			#apt-get install mysql-server

		else if [ $opt_stack == 3 ]; then

			#Instalar ambos PHP y SQL
			apt-get install default-mysql-server php libapache2-mod-php php-mysql

		else
			opt_stack=0
			clear
			echo -e "Porfavor ingrese un numero con las opciones indicadas\n

			"
		fi
		fi
		fi
	done
	exit 0



}

Stack_uninstall() {

	opt_stack=0
	while [ $opt_stack == 0 ]
	do
		echo -e "Seleccione el componente de stack a eliminar\n 
		1= Apache
		2= Php 
		3= Mysql
		4= Php y Mysql"

		read -p "Opcion a eliminar: " opt_stack
		if [ $opt_stack == 1 ]; then

			#Eliminar Apache
			systemctl stop apache2
			apt-get purge apache2 apache2-utils apache2.2-bin apache2-common
			apt-get autoremove --purge

			whereis apache2


			echo -e "Apache ha sido eliminado"

		else if [ $opt_stack == 2  ]; then

			#Eliminar PHP
			apt-get remove php libapache2-mod-php php-mcrypt php-mysql
			echo -e "php ha sido eliminado"

		else if [ $opt_stack == 3 ]; then

			#Eliminar SQl
			apt-get remove mysql-server
			echo -e "Mysql ha sido eliminado"

		else if [ $opt_stack == 4 ]; then

			#Eliminar ambos php y sql
			apt-get remove default-mysql-server php libapache2-mod-php php-mysql

			echo -e "PHP y SQL ha sido eliminado"

		else
			opt_stack=0
			clear
			echo -e "Porfavor ingrese un numero con las opciones indicadas\n

			"
		fi
		fi
		fi
		fi
	done

}

log_site(){

	echo -e "Indique que log deasea visualizar\n 
	1= access.log 
	2= error.log"

	read -p "ingrese su opcion de log: " opt_log

	if [ $opt_log == 1 ]; then

		echo -e "A continuacion los archivos de log de access.log>"
		cat /var/log/apache2/access.log

	else if [ $opt_log == 2 ]; then

		cat /var/log/apache2/error.log




	else 
		clear
		echo "ingreso incorrecto\n 
		vuelve al Menu principal"


	fi
	fi

	exit 0

}

up_down_sites(){
	opt_sites=1
	while [ $opt_sites != 4 ]
	do
		clear
		echo -e "\t*****UP DOWN Menu*****\n
		1. Crear o eliminar directorios
		2. recargar apache
		3. Activar sitio
		4. Salir"
		read -p "Ingrese opcion " opt_sites
		case $opt_sites in
			1) abm_directorios;;
			2) recharge_apache;;
			3) active_site;;
			4) exit 0 ;;

			*) echo -e "Wrong option Selected!!!"
		esac
	done
}

abm_directorios(){

	echo -e "indique el nombre del directorio que desea crear o eliminar\n"
	read -p "El nombre del directorio es: " nombredir


	echo -e "Indique si desea eliminar o crear directorios\n 
	CREAR =1
	ELIMINAR= 2"

	read -p "Ingrese el numero de su opcion: " opt_directorios

	if [ $opt_directorios == 1 ]; then
		clear
		echo -e "Esta por crear un directorio\n
		Indique si quiere Log general o definido por sitio
		Log general = 1
		Personalizado = 2
		"

		read -p "Ingrese su opcion" opt_logs

		if [ $opt_logs == 1 ]; then

			#cat /etc/apache2/sites-available/000-default.conf | grep -v "#" > /etc/apache2/sites-available/$nombredir.conf
			echo -e "<VirtualHost *:80>\n  
			ServerAdmin webmaster@localhost 
			DocumentRoot /var/www/$nombredir   
			servername $nombredir
			ErrorLog /var/log/apache2/error.log
			CustomLog /var/log/apache2/access.log combined
			</VirtualHost>" >> /etc/apache2/sites-available/$nombredir.conf

			systemctl reload apache2
			mkdir -p /var/www/$nombredir

			#le realizamos la carga de algo al sitio index recien creado

			echo "<h1>Hey Hey you are Welcome to $nombredir </h1>" > /var/www/$nombredir/index.html

			echo "El directorio\n 
			/var/www/$nombredir
			ha sido creado"

		else if [ $opt_logs == 2 ]; then

			#cat /etc/apache2/sites-available/000-default.conf | grep -v "#" > /etc/apache2/sites-available/$nombredir.conf
			echo -e "<VirtualHost *:80>\n  
			ServerAdmin webmaster@localhost 
			DocumentRoot /var/www/$nombredir   
			servername $nombredir
			ErrorLog /var/log/apache2/error.log
			CustomLog /var/log/apache2/$nombredir.log combined
			</VirtualHost>" >> /etc/apache2/sites-available/$nombredir.conf

			systemctl reload apache2

			mkdir -p /var/www/$nombredir

			#le realizamos la carga de algo al sitio index recien creado

			echo "<h1>Hey Hey you are Welcome to $nombredir </h1>" > /var/www/$nombredir/index.html

			echo "El directorio\n 
			/var/www/$nombredir
			ha sido creado"

		fi
		fi


	else if [ $opt_directorios == 2 ]; then
		rm -rf  /etc/apache2/sites-available/$nombredir.conf   
		rm -rf /var/www/$nombredir

		echo "El directorio\n 
		/var/www/$nombredir
		ha sido eliminado"
		exit

	fi
	fi

	exit 0	
}

recharge_apache(){

	systemctl reload apache2

	echo -e "Se aplicado la recarga de apache"

	exit 0

}

active_site(){

	echo -e "Indique el sitio que desea activar o desactivar\n
	(Ej. sitio.com.conf "
	read -p "Nombre de sitio a activar: " site_to_active


	echo -e "Indique si desea desactivar o activar el site\n 
	Activar = 1
	Desactivar = 2"

	read -p "Ingrese el numero de su opcion: " opt_abm_site

	if [ $opt_abm_site == 1 ]; then

		echo -e "Se va a activar el siguiente sitio $site_to_active :"
		a2ensite $site_to_active.conf
		echo -e "El sitio $site_to_active ha sido activado"

	else if [ $opt_abm_site == 2 ]; then

		a2dissite $site_to_active.conf   
		echo -e "El sitio $site_to_active ha sido desactivado"

	fi
	fi
	exit 0

}

database_word () {

	mysql -e "CREATE DATABASE wordpress;"

	mysql -e "GRANT ALL ON wordpress.* TO 'user'@'localhost' IDENTIFIED BY '1234';"

	mysql -e "FLUSH PRIVILEGES;"

	echo -e " Basen de datos wordpress creada\n

	Nombre de usuario> user
	Password> 1234
	 
	 "


 }

download_wordpress () {

	cd /var/www/html
	# wget
	wget -P /var/www/html https://wordpress.org/latest.tar.gz
	#wget -p https://wordpress.org/latest.tar.gz
	ls
	#index.html info.php latest.tar.gz
	#Descomprimir wordpress
	tar xf latest.tar.gz
	ls
	#index.html info.php latest.tar.gz wordpress

	cd /home/$USER
	exit 0 
}

config_wordpress () {

	cd /var/www/html/wordpress
	cp wp-config-sample.php wp-config.php

	sed -i "s/$oldip/$newip/g" /etc/network/interfaces
	sed -i "s/database_name_here/wordpress/g" wp-config.php
	sed -i "s/username_here/user/g" wp-config.php
	sed -i "s/password_here/1234/g" wp-config.php

	cd /home/$USER

	echo -e "Puede probar acceso dirigiendose a http://<Ipdelservidor>/wordpress\n"


}


deploy_wordpress () {


	echo -e "\t*****wordpress DOWN Menu*****\n
	1. Base de datos
	2. deploy
	3. config
	4. Salir"
	read opt_word
	case $opt_word in
		1) database_word ;;
		2) download_wordpress ;;
		3) config_wordpress ;;
		4) exit 0 ;;
		*) echo -e "Wrong option Selected!!!"

		esac


	}

main(){

	ROOT_UID=0
	if [ $UID == $ROOT_UID ];
	then
		clear

		opt_main=1
		while [ $opt_main != 8 ]
		do
			echo -e "/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\n" 
			#############APACHE MAIN MENU ############ 
			echo -e "\t*****Main Menu*****\n
			1. Cambiar ip del servidor
			2. Cambiar Hostname
			3. stack install
			4. stack uninstall
			5. Alta/baja de sitios
			6. log
			7. Deploy Wordpress
			8. Salir"
			read opt_main
			case $opt_main in
				1) change_ip ;;
				2) hostname ;;
				3) Stack_install ;;
				4) Stack_uninstall ;;
				5) up_down_sites ;;
				6) log_site ;;
				7) deploy_wordpress ;;
				8) exit 0 ;;

				*) echo -e "Wrong option Selected!!!"
			esac
		done


	else
		echo -e "OPS! Tienes que ser root para continuar"
	fi
}

main
exit 0
